Title: A sample blog post
Date: 2022-09-15

# A sample blog post

Blog posts are often useful on a webpage like this, they show the
webpage is alive, and they help with positioning on search engines
(e.g. Google).
